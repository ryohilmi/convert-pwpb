const siswa = require("../models/index").siswa;
const op = require("sequelize").Op;

module.exports = {
    getMurid: async (req, res) => {
        const data = await siswa.findAll();
        try {
            res.json(data);
        } catch (err) {
            res.json({
                status: "ERROR",
                message: err
            });
        }
    },
    saveMurid: async (req, res) => {
        let { nomor_induk, nama, jenkel, alamat, kelas } = req.body;

        const cekNIS = await siswa.findAll({
            where: {
                nomor_induk
            }
        });

        if (cekNIS.length > 0) {
            res.status(500).json({
                cekNIS
            });
            return;
        }

        try {
            const data = await siswa.create({
                nomor_induk,
                nama,
                jenkel,
                alamat,
                kelas
            });
            res.status(201).json(data);
        } catch (err) {
            res.json({
                status: "ERROR",
                message: err
            });
        }
    },
    updateMurid: async (req, res) => {
        try {
            let { nama, jenkel, alamat, kelas } = req.body;
            const data = await siswa.update(
                {
                    nama,
                    jenkel,
                    alamat,
                    kelas
                },
                {
                    where: { nomor_induk: req.params.nomor }
                }
            );
            res.status(201).json(data);
        } catch (err) {
            res.status(500).json({
                status: "ERROR",
                message: err
            });
        }
    },
    hapusMurid: (req, res) => {
        let ni = req.params.nomor;
        try {
            siswa
                .destroy({
                    where: { nomor_induk: ni }
                })
                .then(() => {
                    res.status(201).json({
                        message: "Success"
                    });
                });
        } catch (err) {
            res.status(500).json({
                status: "ERROR",
                message: err
            });
        }
    },
    cariMurid: async (req, res) => {
        const cari = req.params.cari;

        const data = await siswa.findAll({
            where: {
                [op.or]: [
                    {
                        nomor_induk: {
                            [op.like]: `%${cari}%`
                        }
                    },
                    {
                        nama: {
                            [op.like]: `%${cari}%`
                        }
                    },
                    {
                        kelas: {
                            [op.like]: `%${cari}%`
                        }
                    }
                ]
            }
        });
        try {
            res.json(data);
        } catch (err) {
            res.json({
                status: "ERROR",
                message: err
            });
        }
    }
};
