const express = require('express');
const app = express();
const cors = require('cors');

const routeSiswa = require('./route/siswaRoute');

app.use(express.urlencoded({extended : false}));
app.use(express.json());
app.use(cors());    

app.use('/api', routeSiswa);

app.listen(3000,() => console.log("Listening on port 3000"));