'use strict';
module.exports = (sequelize, DataTypes) => {
    const siswa = sequelize.define('siswa', {
        nomor_induk: {
            primaryKey: true,
            type: DataTypes.INTEGER
        },
        nama: DataTypes.STRING(50),
        jenkel: DataTypes.STRING(15),
        alamat: DataTypes.TEXT,
        kelas: DataTypes.STRING(15)
    }, {
        timestamps: false,
        freezeTableName: true,
        tableName: 'siswa'
    });
    return siswa;
}