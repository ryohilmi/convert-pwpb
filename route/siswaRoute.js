const express = require("express");
const router = express.Router();

const siswa = require("../controller/siswaController");

router.get("/siswa", siswa.getMurid);
router.get("/siswa/:cari", siswa.cariMurid);
router.post("/siswa", siswa.saveMurid);
router.put("/siswa/:nomor", siswa.updateMurid);
router.delete("/siswa/:nomor", siswa.hapusMurid);

module.exports = router;
